<?php
	
	class Admin extends CI_Controller{

		function dashboard(){

			if($this->auth->is_admin()){

				$this->load->model("UserModel");

				$this->UserModel->table = "user";
				
				$all['users'] = $this->UserModel->find_all();

				$this->layout->load("dashboard",$all);

			}else{

				$this->load->view('errors/admin');
			}

		}

		function block(){

			$time =	$this->input->post('block_time');

			$id = $this->input->post('user_id');

			$t = time()+$time*60;

			if(empty($time)){

				$t = 0;
			}

			$this->load->model('UserModel');

			$this->UserModel->table = 'user';

			$this->UserModel->update($id,array('block'=>$t));

			redirect('admin/dashboard');
		}

	}

?>