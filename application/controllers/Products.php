<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller{

	function  __construct() {

    	parent::__construct();
    	$this->load->library('paypal_lib');
    	$this->load->model('product');

    }

    function index(){

        $config['index_page'] = '';
        $data = array();
        //get products data from database
        $data['products'] = $this->product->getRows();
        //pass the products data to view
        $this->load->view('products/index', $data);
       
    }

    function buy($id){
        
        //Set variables for paypal form

        $returnURL = base_url().'paypal/success'; //payment success url

        $cancelURL = base_url().'paypal/cancel'; //payment cancel url

        $notifyURL = base_url().'paypal/ipn'; //ipn url

        //get particular product data

        $product = $this->product->getRows($id);

        $userID = 1; //current user id

        $logo = base_url().'assets/images/codexworld-logo.png';

        

        $this->paypal_lib->add_field('return', $returnURL);

        $this->paypal_lib->add_field('cancel_return', $cancelURL);

        $this->paypal_lib->add_field('notify_url', $notifyURL);

        $this->paypal_lib->add_field('item_name', $product['name']);

        $this->paypal_lib->add_field('custom', $userID);

        $this->paypal_lib->add_field('item_number',  $product['id']);

        $this->paypal_lib->add_field('amount',  $product['price']);        

        $this->paypal_lib->image($logo);

        

        $this->paypal_lib->paypal_auto_form();
    }

     function add(){

            if($this->auth->is_logged()){

                $this->load->view('create_product');

            }else{

                $this->session->set_flashdata('login_message','Please login to continue');

                $this->load->view('login');
            }


        }

        function new_product(){

            $name  = $this->input->post('pname');

            $price  = $this->input->post('pquantity');

            $this->load->library('form_validation');

            $this->form_validation->set_rules('pname','Name',"trim|required",array('required'=>"անունը պարտադիր է"));

            $this->form_validation->set_rules('pquantity','quantity',"trim|required",array('required'=>"նշեք քանակը"));

            if($this->form_validation->Run() == false){
            
                $this->load->view('create_product');
                
            
            }else{

                $config['upload_path'] =  './img/photos/'; 

                $config['allowed_types'] = 'gif|jpg|png|jpeg'; 

                $config['encrypt_name'] = TRUE; 

                $config['remove_spaces'] = TRUE; 

                $this->load->library('upload', $config); 

                $this->upload->do_upload('userfile'); 

                $upload_data = $this->upload->data();

                $photo = $upload_data['file_name'];

                $error = $this->upload->display_errors();

                $data = array(

                    "name" => $name,

                    "image" => $photo,

                    "price" => $price

               );
                
                $this->load->model('Product');
                
                $this->Product->Insert($data);

                redirect('/products/', 'refresh');



            }



        }
    }
?>

