<?php 
 class User extends CI_Controller{

 	function index(){

 		$this->load->view('signup');

 	}
 	function insert(){

 		$name = $this->input->post('name');
 		$surname = $this->input->post('surname');
 		$age = $this->input->post('age');
 		$pass = $this->input->post('pass');
 		$email = $this->input->post('email');

 		$this->load->library('form_validation');

 		$this->form_validation->set_rules('name','Name',"trim|required|min_length[4]|max_length[12]",array('required'=>"դդում, անունդ պապս պիտի գրի՞","min_length"=>"առնվազն 4 տառ",'max_length'=>"առավելագույնը 12 տառ"));


 		$this->form_validation->set_rules('surname','surname',"trim|required|min_length[4]|max_length[25]",array('required'=>"դդում, ազգանունդ պապս պիտի գրի՞","min_length"=>"առնվազն 4 տառ",'max_length'=>"առավելագույնը 25 տառ"));

 		$this->form_validation->set_rules('age','age','required',array('required'=>"դդում, տարիքդ պապս պիտի գրի՞"));

 		$this->form_validation->set_rules('pass','password',"trim|required|min_length[5]|max_length[12]",array('required'=>"դդում, գաղտնաբառը պիտի գրի՞","min_length"=>"առնվազն 5 սիմվոլ",'max_length'=>"առավելագույնը 12 սիմվոլ"));

 		$this->form_validation->set_rules('email','email',"trim|required|valid_email|is_unique[user.email]",array('required'=>"դդում, էլ հասցեդ պապս պիտի գրի՞","valid_email"=>"էլ հասցեի մեջ սխալ ունեք սիրելիս",'is_unique'=>'Ձեր email_ը արդեն գրանցված է'));

 		if($this->form_validation->Run() == false){
 			
 			$this->load->view('signup');
 			
 		}else{

 			$h_pass = password_hash($pass, PASSWORD_DEFAULT);

 			$key = md5($name.$email);

 			$data = array(

 				'name' => $name,

 				'surname' => $surname,

 				'age' => $age,

 				'password' => $h_pass,

 				'email' => $email,

 				'key' => $key,

 				'is_active' => 0

 			);

 			$this->load->model('UserModel');

 			$this->UserModel->Insert($data);

		    $config = Array(
			  'protocol' => 'smtp',
			  'smtp_host' => 'ssl://smtp.googlemail.com',
			  'mail_path' => 'ssl://smtp.googlemail.com',
      		  'smtp_port' => 465,
			  'smtp_user' => 'ed.hovhannisyan99@gmail.com', // change it to yours
			  'smtp_pass' => 'eh160899google', // change it to yours
			  'mailtype' => 'html',
			  'charset' => 'iso-8859-1',
			  'wordwrap' => TRUE
			);

		    $url = base_url("user/activation/$key");
			$message = "To activate your account please <a href=$url >click here </a>";

	   		$this->load->library('email', $config);

	   		$this->email->set_newline("\r\n");

	   		$this->email->from('ed.hovhannisyan99@gmail.com'); // change it to yours

	   		$this->email->to($email);// change it to yours

	   		$this->email->subject('Resume from JobsBuddy for your Job posting');

	   		$this->email->message($message);

       
	      if($this->email->send()){

	      	echo '<h1>we have sent you a confirmation email.</h1>';
	      }
	      else{	     

	       	show_error($this->email->print_debugger());
	      }



       		}


 	}

 	function signin(){

 		$this->load->view('login');
	}

	function login(){
		
		$login = $this->input->post('login');
		$password = $this->input->post('lpassword');
		$this->load->model('UserModel');
		$this->UserModel->table = "user";
		$user = $this->UserModel->find($login);
		$time = time();
		date_default_timezone_set( "Asia/Ashgabat" );

		if(!empty($login)){

		if(!empty($user)){	
			$v_password = password_verify( $password,$user[0]['password'] );

				if($user[0]['block'] < time()){

					$user[0]['block'] = null;
					$this->UserModel->update($user[0]['id'],array('block' => 0));
									
				}else{

					$minutes = intval(($user[0]['block']-$time)/60);

						if($minutes == 0){

							$minutes = "less than 1";
						}

					$this->session->set_flashdata('login_message', "You are blocked for $minutes minute(s)");
					$this->load->view('login');
					
				}

					if($v_password && $user[0]['block'] == NULL && $user[0]['is_active'] != 0 ){

						$data = array(
							'id' => $user[0]['id'],
							'name' => $user[0]['name'],
							'surname' => $user[0]['surname'],
							'age' => $user[0]['age'],
							'email' => $user[0]['email'],
							'type' => $user[0]['type']
						);

						$this->UserModel->update($user[0]['id'],array('enter'=>time()) );
						$this->session->set_userdata('user',$data);
						$this->layout->load('profile',$data);
						
						
					}

					if($user[0]['is_active'] == 0){

						$this->session->set_flashdata('login_message', "Your accout isn't activated yet");
						$this->load->view('login');
						
					}
					if(!$v_password){

						$this->session->set_flashdata('login_message', 'Your Email or password is wrong');
						$this->load->view('login');
						
					}
					
				
			
		}else{

			$this->session->set_flashdata('login_message', 'Your Email or password is wrong');
			$this->load->view('login');
			
		}	
				
	}else{

		$this->load->view('login');
	}

}


	function logout(){

		if($this->session->has_userdata('user')){

			$this->session->unset_userdata('user');

			redirect('user/login');
		}

	}

	function activation(){

		$this->load->model("UserModel");

		$this->UserModel->table = 'user';

		$user = $this->UserModel->find_by('key',$this->uri->segment(3));

		if(!empty($user)){

			$this->UserModel->update($user[0]['id'],array('is_active'=>1));

			print "<h1 style='color:lightgreen'>Your account is now active</h1>";
		}else{

			print "<h1 style='color:darkred'>Something is wrong</h1>";
		}




	}

	function edit(){

		$this->layout->load("edit");
	}
	function change(){

		$name = $this->input->post('new_name');

		$pass = $this->input->post('pass');

		$pin = $this->input->post('pin');

		// var_dump("$name $pass $pin");die;

		$user_data = $this->session->userdata('user');

		$this->load->model("UserModel");

		$this->UserModel->table = 'user';

		if(!empty($name)){
			$this->UserModel->update($user_data['id'],array('name'=>$name));
		}

		if(!empty($pass)){
			$this->UserModel->update($user_data['id'],array('pass'=>$pass));
		}

		if(!empty($pin)){
			$this->UserModel->update($user_data['id'],array('pin'=>$pin));
		}

		if(empty($name) && empty($pass) && empty($pin)){

			$this->session->set_flashdata('edit_message', "Nothing changed");
			$this->edit();
		}else{

			$this->session->set_flashdata('edit_message', "Saved");
			$this->edit();
		}

 		}

 		function forget(){

 			$this->load->model("UserModel");

			$this->UserModel->table = 'user';

 			$user = $this->UserModel->find($this->input->post('mail'));

 			$pin =  strtolower($this->input->post('pin'));

 			

 			if(!empty($user)){

 				$real_pin =  strtolower($user[0]['pin']);
 				
 				if($pin == $real_pin){
 			$config = Array(
			  'protocol' => 'smtp',
			  'smtp_host' => 'ssl://smtp.googlemail.com',
			  'mail_path' => 'ssl://smtp.googlemail.com',
      		  'smtp_port' => 465,
			  'smtp_user' => 'ed.hovhannisyan99@gmail.com', // change it to yours
			  'smtp_pass' => 'eh160899google', // change it to yours
			  'mailtype' => 'html',
			  'charset' => 'iso-8859-1',
			  'wordwrap' => TRUE
			);

	 			$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		  		$pass = array(); //remember to declare $pass as an array
		  		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		  		for ($i = 0; $i <= 8; $i++) {
		  		    $n = rand(0, $alphaLength);
		  		    $pass[] = $alphabet[$n];
		  		}


		  	$new_pass = implode($pass);

		  	$to = $user[0]['email'];

		  	$message = "Your new password is $new_pass";

	   		$this->load->library('email', $config);

	   		$this->email->set_newline("\r\n");

	   		$this->email->from('ed.hovhannisyan99@gmail.com'); // change it to yours

	   		$this->email->to($to);// change it to yours

	   		$this->email->subject('New password');

	   		$this->email->message($message);

	   		$this->UserModel->update($user[0]['id'],array('password'=>password_hash($new_pass, PASSWORD_DEFAULT)));

	   		$this->email->send();

	   		echo 'We have sent you an email';

	   	}else{

	   		echo "your pin code is not real";
	   	}
	      
	  }else{

	  	echo 'Your email is not registered';
	  }

 			
 		}
	}

	

  		
	

?>


				
				
		
			
			


