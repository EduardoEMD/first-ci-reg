<?php 
 class UserModel extends CI_model{

 	public $table = "";

 	function __construct(){

 		parent::__construct();

 	}

 	function Insert($data){

 		$this->db->insert('user',$data);
 	}

 	function find_all($sort = 'id', $order = 'asc'){

        $this->db->order_by($sort, $order);

        $query = $this->db->get($this->table);

        return $query->result_array();

    }

    function find($email){

        $this->db->where("email",$email);

        $query = $this->db->get($this->table);

        return $query->result_array();
        

    }
    function find_by($by, $param){

        $this->db->where("$by",$param);

        $query = $this->db->get($this->table);
        
        return $query->result_array();

    }

    function update($id,$data){

    	$this->db->where('id',$id);

    	$this->db->update($this->table,$data);

    }

    function delete($id){

        if($id != NULL){

            $this->db->where('id', $id);                    

            $this->db->delete($this->table);                        

        }

    }    


 }



?>