<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
  <style type="text/css">
    body{
      background-image: url(http://www.planwallpaper.com/static/images/HD-Wallpapers1_FOSmVKg.jpeg);
      background-position: center;
      background-size: cover;
      background-repeat: no-repeat;
      min-height: 950px;
    }
    .to_right{
      padding: 4%!important;
    }
    #reg_form{
     width: 45%;
    }
    .input-field label{
      color:white;
    }
    #container{
      display: inline;
      width: 100vh;
      height: auto;
    }
    #container>h2{
      text-align: center;
    }
    input{
      color:white;
    }
    .btn, .btn-large{
     background-color: #3F17A0;
    }
    .btn:hover{
      background-color: #9E26A6;
    }
    form+a{
      padding: 4%!important;
    }
  </style>
</head>
<body>
  <div id="container">
    <div id="reg_form">
      <form class="col s6 to_right" action=<?= base_url('user/insert'); ?> method='post'>
        <div class="row">
          <div class="input-field col s12">
            <input id="first_name" type="text" class="validate"  name="name" value="<?php echo set_value('name'); ?>">
            <label for="first_name">First Name</label>
          </div>
          <label style="color:red"><?php echo form_error('name'); ?></label>
        </div>
        <div class="row">
         <div class="input-field col s12">
            <input id="last_name" type="text" class="validate"  name="surname" value="<?php echo set_value('surname'); ?>">
            <label for="last_name">Last Name</label>
          </div>
          <label style="color:red"><?php echo form_error('surname'); ?></label>

        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="age" type="text" class="validate"  name="age" value="<?php echo set_value('age'); ?>">
            <label for="age">Age</label>

          </div>
          <label style="color:red"><?php echo form_error('age'); ?></label>
        </div>

        <div class="row">
          <div class="input-field col s12">
            <input id="password" type="password" class="validate" name="pass">
            <label for="password">Password</label>
          </div>
          <label style="color:red"><?php echo form_error('pass'); ?></label>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="email" type="email" class="validate" name="email" value="<?php echo set_value('email'); ?>">
            <label for="email">Email</label>
          </div>
          <label style="color:red"><?php echo form_error('email'); ?></label>
        </div>
        <button class="btn waves-effect waves-light" type="submit" name="action">Submit
          <i class="material-icons right"></i>
        </button>
      </form>
      <a style="color:white" href=<?= base_url('user/login'); ?>> <button class="btn waves-effect waves-light">already have an accout ?
      </button></a>
       </div>
  </div> 
</body>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
</html>