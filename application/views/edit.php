<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#controller{
			width: 65%;
			height: auto;
			margin: 0 auto;
			border:1px solid gray;
			margin-top: 10%;
			padding: 2%;
		}
	</style>
</head>
<body>

	<div id="controller">
		<form id="editing" action="<?= base_url('user/change') ?>" method='post'>

			<span style="color:lightgreen"><?php print $this->session->flashdata('edit_message'); ?></span>

			<div class="row">
				<div class="input-field col s10">
					<input id="new_name" type="text" class="validate"  name="new_name" value="<?php echo set_value('new_name'); ?>">
					<label for="first_name">New Name</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s10">
					<input id="new_pass" type="text" class="validate"  name="pass" >
					<label for="first_name">New Password</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s10">
					<input id="pin" type="text" class="validate"  name="pin" value="<?php echo set_value('pin'); ?>">
					<label for="pin">Pin Code</label>
				</div>
			</div>
			<input type="submit" class="btn" name="" value="Save changes">
			
		</form>
	</div>
	
</body>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
</html>