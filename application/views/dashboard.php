<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/css/materialize.min.css">
	<style type="text/css">
		#cont_table{
			width: 80%;
			height: auto;
			margin:0 auto;
			margin-top: 10%;
		}
		th,td{
			text-align: center!important;
		}
		#sumbit_block{
		    font-size: 30px;
			border:0;
			background-color: white;
			color: black;
		}
		#sumbit_block:hover{
			background-color: #EEEEEE;
			transition: 300ms;
		}
	</style>
</head>
<body>
	<div id="modal1" class="modal modal-fixed-footer">
		<div class="modal-content">
			<h4>Let's Bann them all 😈</h4>
			<form method="post" action=<?= base_url('admin/block'); ?> >
				<input type="text" name="block_time" placeholder="type how many minutes">
				<label>User id</label><input style="color:red" name="user_id"  id="whichone">
		</div>
		<div class="modal-footer">
			<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
			<input id="sumbit_block" type="submit" value="Block">

		</div>
		</form>

	</div>
	<div id="cont_table">

<table class="striped">
	<tr>
		<th>id</th>
		<th>name</th>
		<th>surname</th>
		<th>age</th>
		<th>email</th>
		<th>Last time entered</th>
		<th>Block</th>
	</tr>
	<?php if(!empty($users)):foreach($users as $user):?>
		<tr>
			<td><?= $user['id']; ?></td>
			<td><?= $user['name']; ?></td>
			<td><?= $user['surname']; ?></td>
			<td><?= $user['age']; ?></td>
			<td><?= $user['email']; ?></td>
			<td><?= date('m/d/Y H:i:s', $user['enter']); ?></td>
			<td><a data-id=<?= $user['id']; ?>  class="waves-effect waves-light btn curse" href="#modal1">Set blocking time</a></td>
		</tr>
	<?php endforeach;endif; ?>
</table> 
<div>



</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/js/materialize.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){

       $('.modal').modal();

        $('.curse').click(function(){

        	userID = $(this).attr('data-id')
        	username = $(this).attr('data-name')
			$('#whichone').val(userID)
        })
       

  });
</script>
</html>