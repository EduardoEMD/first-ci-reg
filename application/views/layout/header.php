<html>
	<head>
		 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
	</head>
	<body>
		
	  <nav>
	    <div class="nav-wrapper">
	      <a href="#" class="brand-logo">Logo</a>
	      <ul id="nav-mobile" class="right hide-on-med-and-down">
	        <?php if($this->auth->is_admin()): ?>
	        	<li><a href="<?= base_url('admin/dashboard'); ?>">Dashboard</a></li>
	        <?php endif; ?>
	        <?php if(!$this->auth->is_admin()): ?>
	        	<li><a href="<?= base_url('user/edit'); ?>">Edit Profile</a></li>
	        <?php endif; ?>
	         <li><a href="<?= base_url('user/logout'); ?>">Logout</a></li>
	      </ul>
	    </div>
	  </nav>



	</body>