<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
	<style type="text/css">
		#container{
			width: 95%;
			height: auto;
			border:1px solid gray;
			margin:0 auto;
			padding: 3%
		}
		.rowe{
			width: 16%;
			display: inline-block;
			height: 250px;
			margin-left: 2%;
		}
		h1{
			text-align: center;
		}
	</style>
</head>
<body>
	<h1>Welcome to our page "Products"</h1>
	<div id="container">
		<?php for($i = 0; $i < count($data); $i++):  ?>
			<div class="rowe">
				<div class="col s4 m2">
					<div class="card">
						<div class="card-image">
							<img src=<?php print base_url()."./img/photos/".$data[$i]['photo']; ?>>
						</div>
						<div class="card-content">
							<p>Product: <?= $data[$i]['name'] ?><p>
							<p>Quantity: <?= $data[$i]['quantity'] ?><p>
							
								</div>
							</div>
						</div>
					</div>
				<?php endfor;  ?>
			</div>
</body>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
<script type="text/javascript">
	
</script>
</html>