<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
  <style type="text/css">
  	#container{
  		width: 50%;
  	}

  </style>
</head>
<body>
  <div id="container">
    <form class="col s6 to_right" action=<?= base_url('products/new_product'); ?> method='post' enctype="multipart/form-data">
        <div class="row">
          <div class="input-field col s12">
            <input id="pname" type="text" class="validate"  name="pname" value="<?php echo set_value('pname'); ?>">
            <label for="pname">Product's name</label>
          </div>
          <label style="color:red"><?php echo form_error('pname'); ?></label>
        </div>
        <div class="row">
         <div class="input-field col s12">
            <input id="pquantity" type="text" class="validate"  name="pquantity" value="<?php echo set_value('pquantity'); ?>">
            <label for="pquantity">Price</label>
          </div>
          <label style="color:red"><?php echo form_error('pquantity'); ?></label>

        </div>
        <div class="row">
          <div class="file-field input-field">
            <div class="btn">
             <span>Upload file</span>
             <input type="file" name="userfile">
          </div>
          <div class="file-path-wrapper">
             <input class="file-path validate" type="text" placeholder="Upload file">
         </div>
            <button class="btn waves-effect waves-light" type="submit" name="action">Save
          <i class="material-icons right"></i>
        </button>
      </form><br>
    </div> 
        

</body>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
</html>