<!DOCTYPE html>
<html>
<head>
	<title></title>
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
  	<style type="text/css">
   #container{
      display: inline;
      width: 50vh;
      height: auto;
    }
    #container>h2{
      text-align: center;
    }
  	#reg_form{
   		width: 35%;
   		margin: 0 auto;    
   		  

    }
    #container >h1{
    	text-align: center;
    }
    .forg_form{
      width: 50%;
      height: auto;
      padding: 5%;
      margin: 0 auto;
      display: none;
    }
    #forg_togl{
      cursor: pointer;
      color: #E41259;
      margin-left: 2%;

    }
    #forg_togl:hover{
      text-decoration: underline;
    }
  	</style>
</head>
<body>
	<div id="container">
	<h1>Log-In</h1>
	<div id="reg_form">
		<span style="color:red"><?php print $this->session->flashdata('login_message'); ?></span>
      <form class="col s6 to_right" action=<?= base_url('user/login'); ?> method='post'>
        <div class="row">
          <div class="input-field col s12">
            <input type="email" class="validate"  name="login" value="<?php echo set_value('login'); ?>">
            <label for="login">Login</label>
          </div>
          <label style="color:red"><?php echo form_error('login'); ?></label>
        </div>

        <div class="row">

          <div class="input-field col s12">
            <input type="password" class="validate"  name="lpassword" value="<?php echo set_value('lpassword'); ?>">
            <label for="password">Password</label>

          </div>
          <label style="color:red"><?php echo form_error('lpassword'); ?></label>

        </div>
      <button class="btn waves-effect waves-light" type="submit" name="action">Login
          <i class="material-icons right"></i>
        </button>
      </form><br>
     <a style="color:white" href=<?= base_url('user/insert'); ?>> <button class="btn waves-effect waves-light">Back to Registration
      </button></a>
    <a id="forg_togl">forgot password</a>
    </div>
  </div>
  <div class="forg_form">
      <span style='color:#E41259'></span>
      <input type="text" placeholder="Enter your Email" id="email_change" >
      <input type="text" placeholder="Enter your Pin code" id="pin_change" >
      <button class="btn" id="send_pass">Send new password</button>
  </div>

</body>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
<script type="text/javascript">
  $(document).ready(()=>{
      $('#forg_togl').click(()=>{
          $('.forg_form').toggle('slow')

      })

      $('#send_pass').click(function(){
          var data = $("#email_change").val()
          var data1 = $("#pin_change").val()
          $.ajax({
              async:false,
              type:"post",
              url: "<?php echo base_url(); ?>user/forget",
              data:{'mail':data,'pin':data1},
              success:function(r){
                $(".forg_form span").html(r)
              }

             
          })
      })
      
      
  })
</script>
</html>